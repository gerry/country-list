import CountryList from './components/CountryList/CountryList';
import './App.css';

function App() {
  return (
    <div className="App">
      <CountryList />
    </div>
  );
}

export default App;
