import { screen, render, fireEvent } from "@testing-library/react";
import CountryList from './CountryList';

describe("CountryList component", () => {
  test("Header should be available in the component", () => {
    render(<CountryList />);
    const headingElement = screen.getByRole("heading", {name: "Country List"});
    expect(headingElement).toBeInTheDocument();
  })
})