import React, { useEffect, useState, useRef } from 'react'
import styles from "./CountryList.module.css"
import Loader from '../Loader/Loader'
//import axios from 'axios'
import Country from '../Country/Country'

const CountryList = () => {
  const [countries, setCountries] = useState([])
  const [isLoading, setIsLoading] = useState(true);
  const [inputValue, setInputValue] = useState('');
  const [selectedKey, setSelectedKey] = useState("name");
  const inputRef = useRef();

  const selectValues = {
    all: "all",
    name: "Country Name",
    region: "Region",
    lang: "Language",
    currency: "Currency"
  }

  const serverUrl = 'https://restcountries.com/v3.1/';

  const fetchFilteredResults = async (url) => {
    try {
      setIsLoading(true);
      //const { data } = await axios.get(url);

      fetch(url)
        .then(response => response.json())
        .then(data => {
          setCountries(data)
          setIsLoading(false);
          console.log(data);
        })
    }
    catch(error) {
      console.log(error);
      setIsLoading(false);
    }
  }

  const searchHandler = () => {

    if(!inputValue) return;

    let url = `${serverUrl}${selectedKey}/${inputValue}`;

    fetchFilteredResults(url);
    setInputValue("");
  }

  useEffect(() => {
    fetchFilteredResults(`${serverUrl}${selectValues['all']}`);
  }, []);

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  return (
    <>
      <h1>Country List</h1>
      <div>
        <input ref={inputRef} className={styles['bigger-font']} type={`text`} value={inputValue} onChange={(e) => setInputValue(e.target.value)} placeholder="Filter by country name" />
        <select className={styles['bigger-font']} onChange={e => setSelectedKey(e.target.value)}>
          {

            Object.keys(selectValues).map(key => (
              <option key={key} value={key}>{selectValues[key]}</option>
            ))
          }
        </select>
        <button className={styles['bigger-font']} type="button" onClick={searchHandler}>Search</button>
      </div>
      {
        isLoading && <Loader />
      }
      <div className={styles['country-list-countainer']}>
        {countries.length > 0 && !isLoading && countries.map((country) => (
          <Country key={country.name.common} country={country} />
        ))}
      </div>
    </>
  )
}

export default CountryList