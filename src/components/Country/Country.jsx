import React, { useState } from 'react'
import styles from "./Country.module.css"
const Country = ({ country }) => {
  const [expanded, setExpanded] = useState(false);

  const getCurrencies = (currencies) => {
    let resuls = "";
    for(let currency in currencies) {
      return currencies[currency].name
    }

    return resuls;
  }

  const getObjectPropertyValues = (obj) => {
    const keys = Object.keys(obj);
    let results = Array(keys.length);

    for(let i = 0; i < keys.length; i++){
      results.push(obj[keys[i]]);
    }
    return results;
  }

  return (
    <div className={styles['country-container']}>
      <h3><span className={styles.flag}>{country.flag}</span> {country.name.common}</h3>
      <p><strong>Population: </strong>{country.population.toLocaleString('en-US')}</p>
      <p><strong>Region: </strong>{country.region}</p>
      <p>
        <strong>Capital: </strong>
          {country.capital && country.capital.map(capital => (
            <span key={capital}>{capital} </span>
          ))}
        </p>

      <button type='button' onClick={() => setExpanded(!expanded)}>view {expanded ? 'less' : "more"}</button>

      {expanded && (
        <>
          <hr />
          <p><strong>Native Name: </strong>{country.name.common}</p>
          {/* <p><strong>Currencies: </strong>{country.name.currencies && getCurrencies(country.name.currencies)}</p> */}
          <p>
            <strong>Languages: </strong>
            {
              country.languages && getObjectPropertyValues(country.languages).map((lang, index) => (
                <span className={styles.item} key={index}>{lang}</span>
              ))
            }
          </p>
          <p>
            <strong>Border Countries: </strong>
            {
              country.borders && country.borders.map((border, index) => (
                <span className={styles.item} key={index}>{border}</span>
              ))
            }
          </p>
          
          <p><strong>Currencies: </strong>
            {
              country.currencies && getObjectPropertyValues(country.currencies).map((currency, index) => (
                <span className={styles.item} key={index}>{currency.name}</span>
              ))
            }
          </p>
        </>
      )}
    </div>
  )
}

export default Country